import time
import random
import threading
import enum
import RPi.GPIO as GPIO

RED=6
GREEN=5
YELLOW=12

class LedLineMode(enum.Enum):
    OFF = enum.auto()
    ON = enum.auto()

    RANDOM_SLOW = enum.auto()
    RANDOM_FAST = enum.auto()

    RED = enum.auto()
    GREEN = enum.auto()
    YELLOW = enum.auto()

led_line_mode = LedLineMode.RANDOM_SLOW
change_timestamp = time.time()

def led_line_thread():
    global led_line_mode, change_timestamp
    leds = [RED, GREEN, YELLOW]
    last_slow = 0
    while True:
        if time.time() - change_timestamp > 10:
            set_line_mode(LedLineMode.RANDOM_SLOW)
        
        if led_line_mode == LedLineMode.OFF:
            GPIO.output(leds, False)

        elif led_line_mode == LedLineMode.ON:
            GPIO.output(leds, True)

        elif led_line_mode == LedLineMode.RANDOM_SLOW:
            # Only change LEDs if more than 1 sec elapsed since last change
            if time.time() - last_slow > 1:
                random.shuffle(leds)
                GPIO.output(leds[0], True)
                GPIO.output(leds[1:], False)
                last_slow = time.time()

        elif led_line_mode == LedLineMode.RANDOM_FAST:
            random.shuffle(leds)
            GPIO.output(leds, False)
            for led in leds:
                GPIO.output(led, True)
                time.sleep(0.1)
                GPIO.output(led, False)

        elif led_line_mode == LedLineMode.RED:
            GPIO.output([GREEN, YELLOW], False)
            GPIO.output(RED, True)

        elif led_line_mode == LedLineMode.GREEN:
            GPIO.output([RED, YELLOW], False)
            GPIO.output(GREEN, True)

        elif led_line_mode == LedLineMode.YELLOW:
            GPIO.output([GREEN, RED], False)
            GPIO.output(YELLOW, True)

        time.sleep(0.1)


def led_line_setup():
    GPIO.setup(RED, GPIO.OUT)
    GPIO.setup(GREEN, GPIO.OUT)
    GPIO.setup(YELLOW, GPIO.OUT)
    GPIO.output(RED, False)
    GPIO.output(GREEN, False)
    GPIO.output(YELLOW, False)

    threading.Thread(target=led_line_thread, daemon=True).start()

def set_line_mode(mode):
    global led_line_mode, change_timestamp
    led_line_mode = mode
    change_timestamp = time.time()
    