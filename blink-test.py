import RPi.GPIO as GPIO  # Import GPIO Module
from time import sleep  # Import sleep Module for timing
import random

BLUE=17
RED=5
GREEN=6
YELLOW=12
BUTTON=27

# Setup
GPIO.setmode(GPIO.BCM)  # Configures pin numbering to Broadcom reference
GPIO.setwarnings(False)  # Disable Warnings
# Set GPIO pins to output
GPIO.setup(BLUE, GPIO.OUT)  
GPIO.setup(RED, GPIO.OUT)
GPIO.setup(GREEN, GPIO.OUT)
GPIO.setup(YELLOW, GPIO.OUT)
# Set initial values
GPIO.output(BLUE, True)
GPIO.output(RED, False)
GPIO.output(GREEN, False)
GPIO.output(YELLOW, False)
# Set up button
GPIO.setup(BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.add_event_detect(BUTTON, GPIO.FALLING, bouncetime=200)

while (True):
    # Wait for button press
    if GPIO.event_detected(BUTTON):
        print("Button Pressed")
        GPIO.output(BLUE, False)
        for RoUnD in range(random.randint(4,7)):
            leds = [RED, GREEN, YELLOW]
            random.shuffle(leds)
            for led in leds:
               GPIO.output(led, True)
               sleep (0.157)
               GPIO.output(led, False)
        GPIO.output(BLUE, True)
