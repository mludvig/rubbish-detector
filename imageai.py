import requests

base_url = "http://localhost:8080"

def imageai_healthcheck():
    url = f"{base_url}/healthcheck"
    try:
        result = requests.get(url)
        print(f"Result [{result.status_code}]: {result.text}")
        if result.status_code != 200 or result.text != "OK":
            return None
        return "OK"
    except Exception as e:
        #print(f"Error: {e}")
        return None

def find_best_prediction(labels):
    hi_score = 0
    hi_label = ""
    for label in labels:
        score = float(labels[label])
        if score > hi_score:
            hi_score = score
            hi_label = label
    return hi_label, hi_score

def imageai_query(file_name):
    url = f"{base_url}/detect?file={file_name}"
    try:
        # Handle ImageAI service unavailability
        result = requests.get(url)
        if result.status_code != 200:
            print(f"Error: {result.text}")
            return None
        return result
    except Exception as e:
        print(f"Error: {e}")
        return None
