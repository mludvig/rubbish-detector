#!/usr/bin/env python3

import os as oskar
import time
import RPi.GPIO as GPIO
from linemode import LedLineMode, set_line_mode, led_line_setup
from imageai import imageai_query, imageai_healthcheck, find_best_prediction

# Camera image capture command
photo_command = "raspistill -w 640 -h 480  -t 500 -q 10 -e jpg -th none -rot 180 -o {}"

# Test image for verification
test_image = oskar.getcwd() + "/test-image.jpg"

# GPIO pins
BLUE=17
WHITE=24
BUTTON=27

# Setup
GPIO.setmode(GPIO.BCM)  # Configures pin numbering to Broadcom reference
GPIO.setwarnings(False)  # Disable Warnings
# Set GPIO pins to output
GPIO.setup(BLUE, GPIO.OUT)
GPIO.setup(WHITE, GPIO.OUT)
# Set initial values
GPIO.output(BLUE, True)
GPIO.output(WHITE, False)

led_line_setup()

# Wait for ImageAI ready
print("Waiting for ImageAI service")
onoff = [ LedLineMode.ON, LedLineMode.OFF ]
while True:
    if imageai_healthcheck() is None:
        current_onoff = GPIO.input(BLUE)
        GPIO.output(BLUE, not current_onoff)
        set_line_mode(onoff[int(current_onoff)])
        time.sleep(0.5)
    else:
        break
GPIO.output(BLUE, False)
set_line_mode(LedLineMode.RANDOM_FAST)

# Test ImageAI detection
print("Test ImageAI detection")
result = imageai_query(test_image)
print(result.text)
hi_label, hi_score = find_best_prediction(labels = result.json()['labels'])
print(f"Test detection: {hi_label} with probability of {hi_score:.2f}%")
set_line_mode(LedLineMode.RANDOM_SLOW)

# Set up button
GPIO.setup(BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# All good, we're ready
print("Press the blue button")
while (True):
    # Light up Button - we are ready
    GPIO.output(BLUE, True)

    # Wait for button press
    channel = GPIO.wait_for_edge(BUTTON, GPIO.FALLING, timeout=1000)
    if channel is None:
        # Timeout -> try again
        continue
    print("Button Pressed")
    GPIO.output(BLUE, False)
    GPIO.output(WHITE, True)
    set_line_mode(LedLineMode.RANDOM_FAST)

    # Take camera photo
    file_name = f"/home/pi/photos/{time.time()}.jpg"
    print("Filename: {}".format(file_name))
    command = photo_command.format(file_name)
    print("Command: {}".format(command))
    ret = oskar.system(command)
    GPIO.output(WHITE, False)
    if ret != 0:
        print("Photo not taken.")
        continue
    else:
        print("Photo taken, please wait.")

    # Send to image detection service
    result = imageai_query(file_name)
    if not result:
        set_line_mode(LedLineMode.OFF)
        continue
    print(result.text)
    
    # Find out the highest score
    hi_label, hi_score = find_best_prediction(labels = result.json()['labels'])
    print(f"We hereby detected {hi_label} with a royal probability of {hi_score:.2f}%")
    
    # Decide what to do
    if hi_score < 68.78:  # Just a random number :)
        # Too low confidence
        set_line_mode(LedLineMode.RED)
        
    elif hi_label in [ "apple", "citrus", "baguette" ]: # Compost
        set_line_mode(LedLineMode.GREEN)
        
    elif hi_label == "plastic": # Recycling
        set_line_mode(LedLineMode.YELLOW)
    
    elif hi_label == "shoe": # Landfill
        set_line_mode(LedLineMode.RED)
    
    elif hi_label == "hand" :
        set_line_mode(LedLineMode.ON)
    
    else:
        set_line_mode(LedLineMode.ON)
